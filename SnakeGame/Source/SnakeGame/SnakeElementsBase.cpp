// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementsBase.h"
#include"Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
// Sets default values
ASnakeElementsBase::ASnakeElementsBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT ( "MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementsBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElementsBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementsBase::SetFirstElementsType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementsBase::HandleBeginOverlap);
}
void ASnakeElementsBase::Interact(AActor* Interactor,bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

void ASnakeElementsBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
					AActor* OtherActor, 
					UPrimitiveComponent* OtherComp, 
					int32 OtherBodyIndex, bool bFromSweep,
					const FHitResult &SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this,OtherActor);
	}
}

void ASnakeElementsBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else 
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}











