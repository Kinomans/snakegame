// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include"SnakeElementsBase.h"
#include "Interactable.h"
// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementsSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElements(1);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElements(int ElementssNum )
{
	for (int i = 0; i < ElementssNum; ++i)
	{
		FVector NewLocation(SnakeElementss.Num() * ElementsSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementsBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementsBase>(SnakeElementsClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElementIndex = SnakeElementss.Add(NewSnakeElem);
		if (ElementIndex == 0)
		{
			NewSnakeElem->SetFirstElementsType();

		}
	}
	
}

void ASnakeBase::Move()
{

	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementsSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementsSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementsSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementsSize;
		break;
	}

	SnakeElementss[0]->ToggleCollision();


	for (int i = SnakeElementss.Num() - 1; i > 0; i--)
	{

		auto CurrentElements = SnakeElementss[i];
		auto PrevElements = SnakeElementss[i - 1];
		FVector PrevLocation = PrevElements->GetActorLocation();
		CurrentElements->SetActorLocation(PrevLocation);

	}
	SnakeElementss[0]->AddActorWorldOffset(MovementVector);
	SnakeElementss[0]->ToggleCollision();
}
 
void ASnakeBase::SnakeElementOverlap(ASnakeElementsBase* OverlappedElement,AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
	    SnakeElementss.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}








 

